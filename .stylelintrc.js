module.exports = {
  extends: ["stylelint-config-standard"],
  plugins: [
    "stylelint-scss", "stylelint-no-unsupported-browser-features"
  ],
  defaultSeverity: "warning",
  rules: {
    "at-rule-blacklist": ["debug"],
    "at-rule-no-unknown": null,
    "color-named": "never",
    "color-hex-case": "upper",
    "color-hex-length": "long",
    "font-family-name-quotes": "always-unless-keyword",
    "function-url-quotes": "always",
    "max-line-length": [
      75, {
        ignore: "non-comments"
      }
    ],
    "max-nesting-depth": 2,
    "property-case": "lower",
    "selector-attribute-quotes": "always",
    "selector-list-comma-space-after": "always-single-line",
    "selector-max-class": 2,
    "selector-max-compound-selectors": 3,
    "string-quotes": "single",
    "string-no-newline": true,

    // SCSS specific rules
    "scss/at-each-key-value-single-line": true,
    "scss/at-extend-no-missing-placeholder": true,
    "scss/at-import-no-partial-leading-underscore": true,
    "scss/at-mixin-argumentless-call-parentheses": "never",
    "scss/at-rule-no-unknown": true,

    // No unsupported browser features
    "plugin/no-unsupported-browser-features": [
      true, {
        severity: "warning",
        browsers: ["defaults", "not ie < 11", "not OperaMini all"]
      }
    ]
  }
};
