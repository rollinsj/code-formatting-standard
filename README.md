# Code Formatting Standard

Files to help you get your projects started with team standard formatting. Geared toward VS Code and its extensions.

---

## Coverage

| Language   | Linter                                                                                                  | Formatter                               | VS Code (extensions)                                                                                                                                                                                 | Git Hooks |
| ---------- | ------------------------------------------------------------------------------------------------------- | --------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------- |
| HTML       | [HTMLHint](https://github.com/htmlhint/HTMLHint)                                                        | [Unibeautify](https://unibeautify.com/) | [vscode-htmlhint](https://marketplace.visualstudio.com/items?itemName=mkaufman.HTMLHint)<br />[VSCode-Unibeautify](https://marketplace.visualstudio.com/items?itemName=Glavin001.unibeautify-vscode) |           |
| Twig       | [TwigCS](https://github.com/friendsoftwig/twigcs)                                                       |                                         | [vscode-twigcs](https://marketplace.visualstudio.com/items?itemName=cerzat43.twigcs)                                                                                                                 |           |
| CSS        | [stylelint](https://stylelint.io/)                                                                      |                                         |                                                                                                                                                                                                      |           |
| SCSS       | [stylelint](https://stylelint.io/)<br />[stylelint-scss](https://github.com/kristerkari/stylelint-scss) |                                         | [vscode-stylelint](https://marketplace.visualstudio.com/items?itemName=shinnn.stylelint)                                                                                                             |           |
| PHP        |                                                                                                         |                                         |                                                                                                                                                                                                      |           |
| JavaScript |                                                                                                         |                                         |                                                                                                                                                                                                      |           |
| Markdown   |                                                                                                         |                                         |                                                                                                                                                                                                      |           |

## Language details

_Coming soon_
